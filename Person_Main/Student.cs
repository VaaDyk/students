﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Student : Person {
    private int Scholarship;
    private double Karma;
    public StudentGroup group; 

    public Student(String SecondName, String FirstName, String LastName, double Karma, int Scholarship) : base(SecondName, FirstName, LastName) {
        this.Karma = Karma;
        this.Scholarship = Scholarship;
    }


    public int GetScholarship() {
        return this.Scholarship;
    }

    public double GetKarma() {
        return this.Karma;
    }

    public void SetScholarship(int value) {
        this.Scholarship = value;
    }

    public void SetKarma(double value) {
        this.Karma = value;
    }

    public void SetGroup(StudentGroup input) {
        this.group = input;
    }
}
