//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Person {
    private String SecondName;
    private String FirstName;
    private String LastName;

    public Person(String SecondName, String FirstName, String LastName) {
        this.SecondName = SecondName;
        this.FirstName = FirstName;
        this.LastName = LastName;
    }


    public String GetSecondName() {
        return this.SecondName;
    }

    public String GetFirstName() {
        return this.FirstName;
    }

    public String GetLastName() {
        return this.LastName;
    }

    public void SetSecondName(String str) {
        this.SecondName = str;
    }

    public void SetFirstName(String str) {
        this.FirstName = str;
    }

    public void SetLastName(String str) {
        this.LastName = str;
    }
}
