﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program {
    static void Main() {
        Student first = new Student("Bileka", "Yaromyr", "Alexandrovich" , 42, 0);
        Student second = new Student("Rudy", "Alex", "Andeevich", 50, 1000);
        Student third = new Student("Da", "Net", "Maybe", -100, 100500);
        StudentGroup SG = new StudentGroup("TA-91", 3);

        SG.Push_back(first);
        SG.Push_back(second);
        SG.Push_back(third);

        System.Console.WriteLine(first.group.ToString());

        SG.Delete(2);

        System.Console.WriteLine(first.group.ToString());
    }
}

